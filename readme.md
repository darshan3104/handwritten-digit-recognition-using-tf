# Handwritten digit recognition model

This project is a demonstration of my understanding towards creation of models using Tensorflow 2.x.

## Tech Stack

**Language:** Python 3.9

**Framework:** Tensorflow 2.x

**Dataset:** MNIST

## Model v1
1. This model consisted of flatten layer and dense layers in it.
2. Model summary:
![alt text](readme/modelv1-summary.png)
3. Model Evaluation:
  - Test loss: 0.1263793408870697
  - Test accuracy: 0.9638000130653381

## Run Locally

Clone the project

```bashssh
  git clone <https/ssh link>
```

Go to the project directory

```bash
  cd handwritten-digit-recognition-using-tf
```

Install dependencies

```bash
  pip install
```

## References

[1] E. R. G, S. M, A. R. G, S. D, T. Keerthi and R. S. R, "MNIST Handwritten Digit Recognition using Machine Learning," 2022 2nd International Conference on Advance Computing and Innovative Technologies in Engineering (ICACITE), Greater Noida, India, 2022.
